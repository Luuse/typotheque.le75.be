---
title: revival
font_family: revival
download_url: fonts/revival/revival.otf
license: 
license_url: 
designers:
 - B3-Lucie Danloy
styles:
 - Regular
paths:
 - fonts/revival/revival.otf
tags:
 - serif
ascender: 800
descender: -200
sample_text: "la typo revival a ete creee a partir d'objets trouves. c'est en me baladant dans les rues de bruxelles que j'ai pu composer cet alphabet!"
scale: paragraph
public: yes
---

 