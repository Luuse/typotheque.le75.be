---
title: Elastic
font_family: Elastic
download_url: fonts/barlow-elastic/Barlow-elasticc.otf
license: 
license_url: 
designers:
 - B2-Dries Hamels, Olivia Marly
 - B2-Florjent Nuhiji 
styles:
 - elastic
paths:
 - fonts/barlow-elastic/Barlow-elasticc.otf
tags:
 - sans-serif
 - fantasy
ascender: 1000
descender: -200
sample_text: "Un matériau solide se déforme lorsque des forces lui sont appliquées."
scale: sentence
public: yes
---

 