---
title: Barlaxent
font_family: Barlaxent
download_url: fonts/barlaxent/Barlaxent.otf
license: SIL
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl
designers:
 - B2-Zephyr Jonnaert et Nassim Gaddari
 - B2-Loana Esperon Vicente
styles:
 - Regular
paths:
 - fonts/barlaxent/Barlaxent.otf
tags:
 - sans-serif
ascender: 1000
descender: -200
sample_text: "(une variante de la barlow), [une nouvelle ponctuation] /de nouveaux glyphes/ des conditions d'utilisations ? * Ronan Deriez, Elise Neirinck, Jerome Coche, Stephane De Groef, Pierre Smeets, Romain Marula, Paul Moriau"
scale: letter
public: yes
---

 
