---
title: simple
date_pub: 2022/01/03, 14:06:26
font_family: simple
download_url: fonts/simple/simple.zip
license: 
license_url: 
designers:
 - B2-Camille Balseau
styles:
 - Regular
paths:
 - fonts/simple/SIMPLE.otf
tags:
 - Sans Serif
ascender: 775
descender: -225
sample_text: "Abc"
scale: letter # letter, sentence, paragraph
public: yes
---

 
