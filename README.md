# typotheque.le75.be

## Alimentation de la typothèque

Rendez-vous sur la **web IDE** de Gitlab: 

![1](imgs_readme/1.png)

Cliquez sur **docs** puis sur les trois petits points à côté de **fonts** afin d'ouvrir la fenêtre d'édition. Cliquez ensuite sur **New directory**:

![2](imgs_readme/2.png) 

Entrez le nom de la typographie puis cliquez sur **Create directory**

Ensuite, cliquez sur le dossier que vous venez de créer, les trois petits points puis **Upload file**:

![3](imgs_readme/3.png)

Sélectionnez votre/vos fichier(s) ttf ou otf puis cliquez sur **ouvrir**.

Vous pouvez maintenant enregistrer vos modifications en cliquant sur **Commit**:

![4](imgs_readme/4.png)

**!!! ATTENTION !!!**, il faut sélectionner **Commit to main branch**! Vous pouvez ensuite cliquer sur Commit.

![5](imgs_readme/5.png)

Attendez quelques secondes que le programme génère les fichiers nécessaire. Un fois cela fait, rechargez la page et vous verrez apparaître un nouveau fichier dans le dossier de la fonte nommé **info-nomDeLaFonte.md**. Cliquez dessus afin de vérifier les informations et d'éventuellement l'éditer. Vous pouvez ajouter une licence, son url, le nom du ou de la designer, un ou plusieurs tags, le texte de démonstration, ainsi que son échelle (*letter* étant le plus gros, *paragraph* le plus petit). 

![6](imgs_readme/6.png)

**Attention**, d'une manière générale, veuillez à respecter la syntaxe existante. Aussi, les tags doivent s'écrire de la même manière que les styles, ie:

```
tags:
 - Serif
 - Sans-serif
```

Une fois vos modifications apportées, n'oubliez pas de changer la ligne `public: no` en `public: yes` afin que la typo soit visible sur le site.  

cliquez de nouveau sur **Commit**. N'oubliez pas encore une fois de sélectionner **Commit to main branch**!

![7](imgs_readme/7.png)

Vous pouvez ensuite vous rendre sur [typotheque.le75.be](typotheque.le75.be) et vous verrez apparaître votre fonte!

![8](imgs_readme/8.png)