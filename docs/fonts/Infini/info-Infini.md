---
title: infini
date_pub: 2022/09/03, 15:33:53
font_family: infini
download_url: fonts/Infini/Infini.zip
license: 
license_url: 
designers:
 - B2-Andre Ferreira
styles:
 - regular
paths:
 - fonts/Infini/infini2.otf
tags:
 - fantasy
 - sans-serif
ascender: 860
descender: -140
sample_text: "ALPHA BRAVO CHARLIE DELTA ECHO FOXTROT GOLF HOTEL INDIA JULIETTE KILO LIMA MIKE NOVEMBER OSCAR PAPA QUEBEC ROMEO SIERRA TANGO UNIFORM VICTOR WISKY XRAY YANKEE ZULU."
scale: sentence # letter, sentence, paragraph
public: yes
---

 
