---
title: Goozette
date_pub: 2021/10/11, 13:09:25
font_family: Goozette
download_url: fonts/goozette/goozette.zip
license: 
license_url: 
designers:
 - 
styles:
 - Regular
paths:
 - fonts/goozette/Goozette.otf
tags:
ascender: 788
descender: -400
sample_text: "Récite moi l'alphabet."
scale: paragraph # letter, sentence, paragraph
public: yes
---

 
