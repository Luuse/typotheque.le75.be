---
title: Oswald
date_pub: 2021/22/11, 10:19:58
font_family: Oswald
download_url: fonts/Oswald/Oswald.zip
license: 
license_url: 
designers:
 - 
styles:
 - Medium
 - Regular
 - Light
paths:
 - fonts/Oswald/Oswald-Medium.ttf
 - fonts/Oswald/Oswald-Regular.ttf
 - fonts/Oswald/Oswald-Light.ttf
tags:
ascender: 1193
descender: -289
sample_text: "Récite moi l'alphabet."
scale: paragraph # letter, sentence, paragraph
public: no
---

 