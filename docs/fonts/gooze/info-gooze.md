---
title: Goozee
font_family: Goozee
download_url: fonts/gooze/Goozeeoutline.otf
license: 
license_url: 
designers:
 - B2-Alexandra Lambert
 - B2-Madeleine Barbaroux 
styles:
 - outline
paths:
 - fonts/gooze/Goozeeoutline.otf
tags:
 - serif
ascender: 742
descender: -258
sample_text: "Goozee"
scale: letter
public: yes
---

 