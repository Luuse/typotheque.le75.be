---
title: Acte
date_pub: 2022/01/03, 14:23:36
font_family: Acte
download_url: fonts/Acte/Acte.zip
license: 
license_url: 
designers:
 - 
styles:
 - Regular
paths:
 - fonts/Acte/SANDRINE EMBRECKX Acte-Regular.otf
tags:
 - Serif
ascender: 700
descender: -200
sample_text: "Le normographe est une plaquette dans laquelle les contours des lettres ont ete evidees pour pouvoir les tracer sur papier."
scale: sentence # letter, sentence, paragraph
public: yes
---

 
