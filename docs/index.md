---
title: home 
---

Bibliothèque numérique *open source* (téléchargement gratuit) dans laquelle sont enregistrées toutes les typographies créées par les étudiants de l’option graphisme. Chaque année de nouvelles créations vont voir le jour et vont alimenter cette collection. C’est un outil très utile pour les étudiants de l’option et leurs projets mais également pour tout ceux intéressés par la typographie. [www.atypo.be](www.atypo.be)  
  

## Credits

This site uses Free Font Library, a tool to manage and display a font collection on the web.

- *Design & Development*: [Luuse](luuse/io)
- *Download*: [Gitlab](gitlab.com/Luuse/Luuse.tools/mkdocks-typotheque)
- *License*: [GNU/GPL](https://www.gnu.org/licenses/gpl.html)
- *Documentation*: luuse.io/freefontlib
- *Featuring*: OpenTypeJs

